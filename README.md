# js-template

A template to be used as a **JavaScript web application**.

## Included in this template

* NPM configuration (basically, the contents of our old trusty `package.json`)
* Vitest setup (tests are executed using `npm test`)
* Vite configuration (with an HTML template and an SVG)

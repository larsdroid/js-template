export default {
    build: {
        emptyOutDir: true,
        outDir: '../dist'
    },
    root: 'src',
    publicDir: '../public'
}

import { expect, test } from 'vitest'
import { f } from '../src/js'

test('f should return 42', () => {
    expect(f()).toBe(42)
})
